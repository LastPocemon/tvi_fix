<?php

use App\Kernel;
use App\JsonResponseFormatter;
use Doctrine\DBAL\DriverManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

define('ROOT_DIR', dirname(__DIR__) . '');
require ROOT_DIR . '/vendor/autoload.php';

try {
    (new Dotenv())->bootEnv(ROOT_DIR . '/.env');

    $container = new ContainerBuilder();
    (new YamlFileLoader($container, new FileLocator(ROOT_DIR)))
        ->load('config/services.yaml');
    $container->set('entityManager', EntityManager::create(
        DriverManager::getConnection([
            'url' => 'mysql://' . $_ENV['MYSQL_USER'] . ':' . $_ENV['MYSQL_PASSWORD'] . '@tvi_db/tvi'
        ]),
        Setup::createAnnotationMetadataConfiguration(
            [
                ROOT_DIR . '/src/Shop/Domain/Entity',
                ROOT_DIR . '/src/Pay/Domain/Entity'
            ],
            true,
            null,
            null,
            false
        )
    ));
    $container->compile(true);
    $container->set('container', $container);

    /** @var Kernel $app */
    $app = $container->get('kernel');
    $response = $app->handle(Request::createFromGlobals());
} catch (Exception $e) {
    $response = JsonResponseFormatter::internalServerError();
}

$response->send();