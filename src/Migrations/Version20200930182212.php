<?php

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

class Version20200930182212 extends AbstractMigration {

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void {
        $this->addSql(
            'CREATE TABLE tvi.orders (
                id INT NOT NULL AUTO_INCREMENT,
                status VARCHAR(10) NOT NULL,
                price INT NOT NULL,
                PRIMARY KEY (id))'
        );

        $this->addSql(
            'CREATE TABLE tvi.orders_products (
                id_order INT NOT NULL,
                id_product INT NOT NULL,
                quantity INT NOT NULL,
                PRIMARY KEY (id_order, id_product))'
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void {
        $this->addSql('DROP TABLE tvi.orders');
        $this->addSql('DROP TABLE tvi.orders_products');
    }
}