<?php

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

class Version20200930184757 extends AbstractMigration {

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void {
        $this->addSql(
            'CREATE TABLE tvi.products (
                id INT NOT NULL AUTO_INCREMENT,
                name VARCHAR(100) NOT NULL,
                price INT NOT NULL,
                PRIMARY KEY (id))'
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void {
        $this->addSql('DROP TABLE tvi.products');
    }
}