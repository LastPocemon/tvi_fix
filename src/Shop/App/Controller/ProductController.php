<?php

namespace App\Shop\App\Controller;

use App\Controller;
use App\JsonResponseFormatter;
use App\Core\Exception\AppServiceException;
use App\Shop\App\Service\ProductService;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Контроллер для взаимодействия с продуктами
 */
class ProductController extends Controller {

    /** @var ProductService */
    private $productService;

    /**
     * @param ProductService $productService
     */
    public function __construct(ProductService $productService) {
        $this->productService = $productService;
    }

    /**
     * Генерация набора продуктов
     *
     * @return JsonResponse
     */
    public function createProducts(): JsonResponse {
        try {
            return JsonResponseFormatter::ok($this->productService->createProducts());
        } catch (AppServiceException $e) {
            return JsonResponseFormatter::internalServerError($e->getMessage());
        }
    }
}