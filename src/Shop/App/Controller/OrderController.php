<?php

namespace App\Shop\App\Controller;

use App\Controller;
use App\Exception\AppServiceBadRequestException;
use App\JsonResponseFormatter;
use App\Core\Exception\AppServiceException;
use App\Shop\App\Service\OrderService;
use App\Shop\Domain\DTO\OrderProductDTO;
use App\Shop\Domain\Factory\OrderProductDTOFactory;
use App\Shop\Domain\Factory\ProductDTOFactory;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Контроллер для взаимодействия с заказами
 */
class OrderController extends Controller {

    /** @var OrderService */
    private $orderService;

    /**
     * @param OrderService $orderService
     */
    public function __construct(OrderService $orderService) {
        $this->orderService = $orderService;
    }

    /**
     * Создание заказа
     *
     * @return JsonResponse
     */
    public function createOrder(): JsonResponse {
        try {
            return JsonResponseFormatter::ok([
                $this->orderService->create(
                    $this->getOrderProducts()
                )
            ]);
        } catch (AppServiceException $e) {
            return JsonResponseFormatter::internalServerError($e->getMessage());
        } catch (AppServiceBadRequestException $e) {
            throw new BadRequestException($e->getMessage());
        }
    }

    /**
     * Получает данные запроса для создания заказа
     *
     * @return OrderProductDTO[]
     */
    private function getOrderProducts(): array {
        if (!$this->request->attributes->has('products')) {
            throw new BadRequestException('В теле запроса отсутствует параметр "products"');
        }

        $products = $this->request->attributes->get('products');

        $orderProducts = [];
        foreach ($products as $productId => $quantity) {
            if (!is_int($productId) || $productId < 0) {
                throw new BadRequestException('Невалидный Id продукта - ' . $productId);
            }
            if (!is_int($quantity) || $quantity < 0) {
                throw new BadRequestException('Невалидное количество продукта - ' . $quantity);
            }
            $orderProducts[] = OrderProductDTOFactory::create($quantity, ProductDTOFactory::createById($productId));
        }

        return $orderProducts;
    }
}