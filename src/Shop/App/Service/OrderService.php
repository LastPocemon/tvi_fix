<?php

namespace App\Shop\App\Service;

use App\Core\Exception\AppServiceException;
use App\Exception\AppServiceBadRequestException;
use App\Shop\App\Factory\OrderViewFactory;
use App\Shop\App\View\OrderView;
use App\Shop\Domain\DTO\OrderProductDTO;
use App\Shop\Domain\Factory\OrderDTOFactory;
use App\Shop\Domain\Service\CreateOrderService;
use App\Shop\Domain\Service\Exception\DomainBadRequestException;
use App\Shop\Domain\Service\Exception\DomainServiceException;
use Monolog\Logger;

/**
 * Сервис для управления заказами
 */
class OrderService {

    /** @var CreateOrderService */
    private $createOrderService;

    /** @var Logger */
    private $logger;

    /**
     * @param CreateOrderService $createOrderService
     * @param Logger             $logger
     */
    public function __construct(
        CreateOrderService $createOrderService,
        Logger $logger
    ) {
        $this->createOrderService = $createOrderService;
        $this->logger = $logger;
    }

    /**
     * Создание заказа
     *
     * @param OrderProductDTO[] $orderProducts
     *
     * @return OrderView
     * @throws AppServiceException
     * @throws AppServiceBadRequestException
     */
    public function create(array $orderProducts): OrderView {
        try {
            $orderDTO = OrderDTOFactory::createByOrderProducts($orderProducts);
            return OrderViewFactory::createByOrderDTO(
                $this->createOrderService->create($orderDTO)
            );
        } catch (DomainServiceException $e) {
            $message = 'Ошибка создания заказа - ' . $e->getScreenMessage();
            $this->logger->error($e->getMessage(), $e->getTrace());
            throw new AppServiceException($message);
        } catch (DomainBadRequestException $e) {
            throw new AppServiceBadRequestException('Ошибка создания заказа - ' . $e->getMessage());
        }
    }
}