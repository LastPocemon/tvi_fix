<?php

namespace App\Shop\App\Service;

use App\Core\Exception\AppServiceException;
use App\Shop\App\Factory\ProductViewFactory;
use App\Shop\App\View\ProductView;
use App\Shop\Domain\Factory\ProductDTOFactory;
use App\Shop\Domain\Service\CreateProductService;
use App\Shop\Domain\Service\Exception\DomainServiceException;
use Monolog\Logger;

/**
 * Сервис для управления продуктами
 */
class ProductService {

    /** кол-во генерируемых продуктов */
    private const QUANTITY_OF_PRODUCTS_CREATED = 20;

    /** @var CreateProductService */
    private $createProductService;

    /** @var Logger */
    private $logger;

    /**
     * @param CreateProductService $createProductService
     * @param Logger               $logger
     */
    public function __construct(
        CreateProductService $createProductService,
        Logger $logger
    ) {
        $this->createProductService = $createProductService;
        $this->logger = $logger;
    }

    /**
     * Генерирует 20 продуктов
     *
     * @return ProductView[]
     * @throws AppServiceException
     */
    public function createProducts(): array {
        try {
            $productsDTO = array_map(
                function() {
                    $productDTO = ProductDTOFactory::create(
                        str_shuffle('abcdefghijklmnopqrstuvwxyz'),
                        random_int(100, 10000)
                    );
                    return $this->createProductService->create($productDTO);
                },
                range(1, self::QUANTITY_OF_PRODUCTS_CREATED)
            );
        } catch (DomainServiceException $e) {
            $message = 'Ошибка генерации продуктов - ' . $e->getScreenMessage();
            $this->logger->error($e->getMessage(), $e->getTrace());
            throw new AppServiceException($message);
        }

        $productsView = [];
        foreach ($productsDTO as $productDTO) {
            $productsView[] = ProductViewFactory::createByProductDTO($productDTO);
        }
        return $productsView;
    }
}