<?php

namespace App\Shop\App\View;

use JsonSerializable;

/**
 * Модель отображения заказа
 */
class OrderView implements JsonSerializable {

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $status;

    /**
     * @var int
     */
    private $price;

    /**
     * @var OrderProductView[]
     */
    private $products;

    /**
     * @param int                $id
     * @param string             $status
     * @param int                $price
     * @param OrderProductView[] $products
     */
    public function __construct(int $id, string $status, int $price, array $products) {
        $this->id = $id;
        $this->status = $status;
        $this->price = $price;
        $this->products = $products;
    }

    /**
     * {@inheritDoc}
     *
     * @return array<string, int|string|OrderProductView[]>
     */
    public function jsonSerialize(): array {
        return [
            'id'       => $this->id,
            'products' => $this->products,
            'price'    => $this->price,
            'status'   => $this->status,
        ];
    }

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getStatus(): string {
        return $this->status;
    }

    /**
     * @return int
     */
    public function getPrice(): int {
        return $this->price;
    }

    /**
     * @return OrderProductView[]
     */
    public function getProducts(): array {
        return $this->products;
    }
}