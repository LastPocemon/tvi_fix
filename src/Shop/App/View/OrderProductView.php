<?php

namespace App\Shop\App\View;

use JsonSerializable;

/**
 * Модель отображения продуктов заказа
 */
class OrderProductView implements JsonSerializable {

    /**
     * @var int
     */
    private $quantity;

    /**
     * @var ProductView
     */
    private $product;

    /**
     * @param int         $quantity
     * @param ProductView $product
     */
    public function __construct(int $quantity, ProductView $product) {
        $this->quantity = $quantity;
        $this->product = $product;
    }

    /**
     * {@inheritDoc}
     *
     * @return array<string, int|ProductView>
     */
    public function jsonSerialize(): array {
        return [
            'product' => $this->product,
            'quantity' => $this->quantity,
        ];
    }

    /**
     * @return int
     */
    public function getQuantity(): int {
        return $this->quantity;
    }

    /**
     * @return ProductView
     */
    public function getProduct(): ProductView {
        return $this->product;
    }
}