<?php

namespace App\Shop\App\View;

use JsonSerializable;

/**
 * Модель отображения данных продукта
 */
class ProductView implements JsonSerializable {

    /** @var int */
    private $id;

    /** @var string */
    private $name;

    /** @var int */
    private $price;

    /**
     * @param int    $id
     * @param string $name
     * @param int    $price
     */
    public function __construct(int $id, string $name, int $price) {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
    }

    /**
     * {@inheritDoc}
     *
     * @return array<string, int|string>
     */
    public function jsonSerialize(): array {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'price' => $this->price,
        ];
    }

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getPrice(): int {
        return $this->price;
    }
}