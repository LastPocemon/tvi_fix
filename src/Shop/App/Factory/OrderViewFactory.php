<?php

namespace App\Shop\App\Factory;

use App\Shop\App\View\OrderView;
use App\Shop\Domain\DTO\OrderDTO;
use App\Shop\Domain\DTO\OrderProductDTO;

/**
 * Фабрика OrderView
 */
class OrderViewFactory {

    /**
     * Формирование на основе OrderDTO
     *
     * @param OrderDTO $orderDTO
     *
     * @return OrderView
     */
    public static function createByOrderDTO(OrderDTO $orderDTO): OrderView {
        return new OrderView(
            $orderDTO->getId(),
            $orderDTO->getStatus(),
            $orderDTO->getPrice(),
            array_map(
                static function(OrderProductDTO $orderProductDTO) {
                    return OrderProductViewFactory::createByOrderProductDTO($orderProductDTO);
                },
                $orderDTO->getProducts()
            )
        );
    }
}