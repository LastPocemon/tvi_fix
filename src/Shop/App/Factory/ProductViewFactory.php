<?php

namespace App\Shop\App\Factory;

use App\Shop\App\View\ProductView;
use App\Shop\Domain\DTO\ProductDTO;

/**
 * Фабрика отображения продукта на основании данных продуктов
 */
class ProductViewFactory {

    /**
     * Формирование на основе ProductDTO
     *
     * @param ProductDTO $productDTO
     *
     * @return ProductView
     */
    static public function createByProductDTO(ProductDTO $productDTO): ProductView {
        return new ProductView(
            $productDTO->getId(),
            $productDTO->getName(),
            $productDTO->getPrice()
        );
    }
}