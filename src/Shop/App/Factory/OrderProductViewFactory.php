<?php

namespace App\Shop\App\Factory;

use App\Shop\App\View\OrderProductView;
use App\Shop\Domain\DTO\OrderProductDTO;

/**
 * Фабрика OrderProductView
 */
class OrderProductViewFactory {

    /**
     * Формирование на основе OrderProductDTO
     *
     * @param OrderProductDTO $orderProductDTO
     *
     * @return OrderProductView
     */
    public static function createByOrderProductDTO(OrderProductDTO $orderProductDTO): OrderProductView {
        return new OrderProductView(
            $orderProductDTO->getQuantity(),
            ProductViewFactory::createByProductDTO($orderProductDTO->getProduct())
        );
    }
}