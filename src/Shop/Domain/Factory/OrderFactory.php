<?php

namespace App\Shop\Domain\Factory;

use App\Shop\Domain\Entity\Order;
use App\Shop\Domain\Entity\OrderProduct;

/**
 * Фабрика заказа
 */
class OrderFactory {

    /**
     * Формирует заказ
     *
     * @param OrderProduct[] $products
     *
     * @return Order
     */
    public static function create(array $products): Order {
        $order = new Order();
        foreach ($products as $orderProduct) {
            $orderProduct->setOrder($order);
        }
        return $order->setProducts(...$products);
    }
}