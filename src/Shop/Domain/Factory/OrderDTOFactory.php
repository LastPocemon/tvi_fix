<?php

namespace App\Shop\Domain\Factory;

use App\Shop\Domain\DTO\OrderDTO;
use App\Shop\Domain\DTO\OrderProductDTO;

/**
 * Фабрика данных заказа
 */
class OrderDTOFactory {

    /**
     * Формирует данные заказа
     *
     * @param OrderProductDTO[] $orderProducts
     *
     * @return OrderDTO
     */
    public static function createByOrderProducts(array $orderProducts): OrderDTO {
        return (new OrderDTO())->setProducts($orderProducts);
    }
}