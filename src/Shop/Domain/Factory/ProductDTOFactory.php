<?php

namespace App\Shop\Domain\Factory;

use App\Shop\Domain\DTO\ProductDTO;

/**
 * Фабрика данных продуктов
 */
class ProductDTOFactory {

    /**
     * Создает новый продукт
     *
     * @param string $name
     * @param int    $price
     *
     * @return ProductDTO
     */
    public static function create(string $name, int $price): ProductDTO {
        return (new ProductDTO())
            ->setName($name)
            ->setPrice($price);
    }

    /**
     * @param int $id
     *
     * @return ProductDTO
     */
    public static function createById(int $id): ProductDTO {
        return (new ProductDTO())
            ->setId($id);
    }
}