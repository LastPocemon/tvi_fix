<?php

namespace App\Shop\Domain\Factory;

use App\Shop\Domain\Entity\Product;

/**
 * Фабрика продуктов
 */
class ProductFactory {

    /**
     * Формирование продукта по данным
     *
     * @param string $name
     * @param int    $price
     *
     * @return Product
     */
    public static function create(string $name, int $price): Product {
        return (new Product())
            ->setName($name)
            ->setPrice($price);
    }
}