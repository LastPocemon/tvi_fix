<?php

namespace App\Shop\Domain\Factory;

use App\Shop\Domain\DTO\OrderProductDTO;
use App\Shop\Domain\DTO\ProductDTO;

/**
 * Фабрика данных записи продукта заказа
 */
class OrderProductDTOFactory {

    /**
     * Формирует запись продукта для нового заказа
     *
     * @param int        $quantity
     * @param ProductDTO $productDTO
     *
     * @return OrderProductDTO
     */
    public static function create(int $quantity, ProductDTO $productDTO): OrderProductDTO {
        return (new OrderProductDTO())
            ->setQuantity($quantity)
            ->setProduct($productDTO);
    }
}