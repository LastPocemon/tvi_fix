<?php

namespace App\Shop\Domain\Factory;

use App\Shop\Domain\Entity\OrderProduct;
use App\Shop\Domain\Entity\Product;

/**
 * Фабрика создания записи продукта для заказа
 */
class OrderProductFactory {

    /**
     * Формирует запись продукта для заказа
     *
     * @param int     $quantity
     * @param Product $product
     *
     * @return OrderProduct
     */
    public static function create(
        int $quantity,
        Product $product
    ): OrderProduct {
        return (new OrderProduct())
            ->setQuantity($quantity)
            ->setProduct($product);
    }
}