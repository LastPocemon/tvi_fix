<?php

namespace App\Shop\Domain\Repository;

use App\Shop\Domain\Entity\Order;
use App\Shop\Domain\Repository\Exception\RepositorySaveException;
use Doctrine\ORM\EntityManagerInterface;
use Throwable;

/**
 * Репозиторий хранения продуктов (Doctrine)
 */
class OrderRepository implements OrderRepositoryInterface {

    /** код для исключений */
    private const EXCEPTION_CODE = 2;

    /** имя сущности для исключений */
    private const ENTITY_NAME = 'Order';

    /** @var EntityManagerInterface */
    private $entityRepository;

    /**
     * @param EntityManagerInterface $entityRepository
     */
    public function __construct(EntityManagerInterface $entityRepository) {
        $this->entityRepository = $entityRepository;
    }

    /**
     * @inheritDoc
     */
    public function save(Order $order): Order {
        try {
            $this->entityRepository->persist($order);
            $this->entityRepository->flush();
            return $order;
        } catch (Throwable $e) {
            throw new RepositorySaveException(
                self::ENTITY_NAME,
                self::EXCEPTION_CODE,
                $e
            );
        }
    }
}