<?php

namespace App\Shop\Domain\Repository\Exception;

use App\Core\Exception\ScreenMessageException;
use Throwable;

/**
 * Исключение репозиториев при получении сущности из репозитория
 */
class RepositoryFindException extends ScreenMessageException {

    /**
     * @param string    $entityName
     * @param int       $code
     * @param Throwable $previous
     */
    public function __construct(string $entityName, int $code, Throwable $previous) {
        parent::__construct(
            $previous->getMessage(),
            $code,
            $previous
        );
        $this->screenMessage = 'Ошибка получения ' . $entityName;
    }
}