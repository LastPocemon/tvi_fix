<?php

namespace App\Shop\Domain\Repository;

use App\Shop\Domain\Entity\Product;
use App\Shop\Domain\Repository\Exception\RepositoryFindException;
use App\Shop\Domain\Repository\Exception\RepositorySaveException;
use Doctrine\ORM\EntityManagerInterface;
use Throwable;

/**
 * Репозиторий хранения продуктов (Doctrine)
 */
class ProductRepository implements ProductRepositoryInterface {

    /** код для исключений */
    private const EXCEPTION_CODE = 1;

    /** имя сущности для исключений */
    private const ENTITY_NAME = 'Product';

    /** @var EntityManagerInterface */
    private $entityRepository;

    /**
     * @param EntityManagerInterface $entityRepository
     */
    public function __construct(EntityManagerInterface $entityRepository) {
        $this->entityRepository = $entityRepository;
    }

    /**
     * @inheritDoc
     */
    public function save(Product $product): Product {
        try {
            $this->entityRepository->persist($product);
            $this->entityRepository->flush();
            return $product;
        } catch (Throwable $e) {
            throw new RepositorySaveException(
                self::ENTITY_NAME,
                self::EXCEPTION_CODE,
                $e
            );
        }
    }

    /**
     * @inheritdoc
     */
    public function findOneById(int $productId): ?Product {
        try {
            return $this->entityRepository->find(Product::class, $productId);
        } catch (Throwable $e) {
            throw new RepositoryFindException(
                self::ENTITY_NAME,
                self::EXCEPTION_CODE,
                $e
            );
        }
    }

    /**
     * @inheritdoc
     */
    public function findManyByIds(array $productIds): array {
        $products = [];
        foreach ($productIds as $productId) {
            $product = $this->findOneById($productId);
            if ($product !== null) {
                $products[] = $product;
            }
        }
        return $products;
    }
}