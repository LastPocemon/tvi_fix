<?php

namespace App\Shop\Domain\Repository;

use App\Shop\Domain\Entity\Order;
use App\Shop\Domain\Repository\Exception\RepositorySaveException;

/**
 * Интерфейс репозитория для сущности заказ
 */
interface OrderRepositoryInterface {

    /**
     * Сохранение заказа
     *
     * @param Order $order
     *
     * @return Order
     * @throws RepositorySaveException
     */
    public function save(Order $order): Order;

}