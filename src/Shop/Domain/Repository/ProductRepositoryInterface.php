<?php

namespace App\Shop\Domain\Repository;

use App\Shop\Domain\Entity\Product;
use App\Shop\Domain\Repository\Exception\RepositorySaveException;
use App\Shop\Domain\Repository\Exception\RepositoryFindException;

/**
 * Интерфейс репозитория для сущности продукт
 */
interface ProductRepositoryInterface {

    /**
     * Сохранение продукта
     *
     * @param Product $product
     *
     * @return Product
     * @throws RepositorySaveException
     */
    public function save(Product $product): Product;

    /**
     * Получение нескольких продуктов по списку ID
     *
     * @param int $productId
     *
     * @return Product|null
     * @throws RepositoryFindException
     */
    public function findOneById(int $productId): ?Product;

    /**
     * @param array<int, int> $productIds
     *
     * @return Product[]
     * @throws RepositoryFindException
     */
    public function findManyByIds(array $productIds): array;

}