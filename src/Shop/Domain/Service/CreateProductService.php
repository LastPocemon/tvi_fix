<?php

namespace App\Shop\Domain\Service;

use App\Shop\Domain\DTO\ProductDTO;
use App\Shop\Domain\Factory\ProductFactory;
use App\Shop\Domain\Repository\Exception\RepositorySaveException;
use App\Shop\Domain\Repository\ProductRepositoryInterface;
use App\Shop\Domain\Service\Exception\DomainServiceException;

/**
 * Сервис для создания новых продуктов
 */
class CreateProductService {

    /** @var ProductRepositoryInterface */
    private $productRepository;

    /**
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(ProductRepositoryInterface $productRepository) {
        $this->productRepository = $productRepository;
    }

    /**
     * Создание нового продукта
     *
     * @param ProductDTO $productDTO
     *
     * @return ProductDTO
     * @throws DomainServiceException
     */
    public function create(ProductDTO $productDTO): ProductDTO {
        $product = ProductFactory::create(
            $productDTO->getName(),
            $productDTO->getPrice()
        );
        try {
            $product = $this->productRepository->save($product);
        } catch (RepositorySaveException $e) {
            throw new DomainServiceException($e);
        }
        return $productDTO->setId($product->getId());
    }
}