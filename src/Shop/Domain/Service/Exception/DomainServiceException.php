<?php

namespace App\Shop\Domain\Service\Exception;

use App\Core\Exception\ScreenMessageException;
use Throwable;

/**
 * Исключение сервиса уровня бизнес-логики
 */
class DomainServiceException extends ScreenMessageException {

    /**
     * @param Throwable $previous
     * @param string    $screen
     */
    public function __construct(Throwable $previous, string $screen = '') {
        parent::__construct(
            $previous->getMessage(),
            $previous->getCode(),
            $previous
        );
        $this->screenMessage = (empty($screen) && $previous instanceof ScreenMessageException) ?
            $this->screenMessage = $previous->getScreenMessage()
            : $screen;
    }
}