<?php

namespace App\Shop\Domain\Service\Exception;

use Exception;

/**
 * Исключение сервиса уровня бизнес-логики из-за некоректных входящих данных
 */
class DomainBadRequestException extends Exception {

}