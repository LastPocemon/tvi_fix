<?php

namespace App\Shop\Domain\Service;

use App\Shop\Domain\DTO\OrderDTO;
use App\Shop\Domain\DTO\OrderProductDTO;
use App\Shop\Domain\Entity\OrderProduct;
use App\Shop\Domain\Factory\OrderFactory;
use App\Shop\Domain\Factory\OrderProductFactory;
use App\Shop\Domain\Repository\Exception\RepositoryFindException;
use App\Shop\Domain\Repository\Exception\RepositorySaveException;
use App\Shop\Domain\Repository\OrderRepositoryInterface;
use App\Shop\Domain\Repository\ProductRepositoryInterface;
use App\Shop\Domain\Service\Exception\DomainBadRequestException;
use App\Shop\Domain\Service\Exception\DomainServiceException;

/**
 * Сервис создания нового заказа
 */
class CreateOrderService {

    /** @var OrderRepositoryInterface  */
    private $orderRepository;

    /** @var ProductRepositoryInterface */
    private $productRepository;

    /**
     * @param OrderRepositoryInterface   $orderRepository
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        OrderRepositoryInterface $orderRepository,
        ProductRepositoryInterface $productRepository
    ) {
        $this->orderRepository = $orderRepository;
        $this->productRepository = $productRepository;
    }

    /**
     * Создает новый заказ
     *
     * @param OrderDTO $orderDTO
     *
     * @return OrderDTO
     * @throws DomainServiceException
     * @throws DomainBadRequestException
     */
    public function create(OrderDTO $orderDTO): OrderDTO {
        try {
            $order = $this->orderRepository->save(
                OrderFactory::create(
                    $this->prepareOrderProducts(
                        $orderDTO->getProducts()
                    )
                )
            );
        } catch (RepositorySaveException $e) {
            throw new DomainServiceException($e);
        } catch (RepositoryFindException $e) {
            throw new DomainServiceException($e);
        }

        return $orderDTO->setId($order->getId())
            ->setStatus($order->getStatus())
            ->setPrice($order->getPrice());
    }

    /**
     * Перерабатывает массив OrderProductDTO в массив OrderProduct
     *
     * @param OrderProductDTO[] $orderProductsDTO
     *
     * @return OrderProduct[]
     * @throws RepositoryFindException
     * @throws DomainBadRequestException
     */
    private function prepareOrderProducts(array $orderProductsDTO): array {
        $productIds = array_map(
            static function(OrderProductDTO $orderProductDTO) {
                return $orderProductDTO->getProduct()->getId();
            },
            $orderProductsDTO
        );

        $products = $this->productRepository->findManyByIds($productIds);

        if (count($products) !== count($orderProductsDTO)) {
            throw new DomainBadRequestException('Не найден один из продуктов');
        }

        $orderProducts = [];
        foreach ($orderProductsDTO as $key => $orderProductDTO) {
            $orderProductDTO->getProduct()
                ->setName($products[$key]->getName())
                ->setPrice($products[$key]->getPrice());
            $orderProducts[] = OrderProductFactory::create(
                $orderProductDTO->getQuantity(),
                $products[$key]
            );
        }

        return $orderProducts;
    }
}