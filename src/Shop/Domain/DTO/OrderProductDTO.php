<?php

namespace App\Shop\Domain\DTO;

/**
 * Данные записи продуктов заказа
 */
class OrderProductDTO {

    /**
     * @var ProductDTO
     */
    private $product;

    /**
     * @var int
     */
    private $quantity;

    /**
     * @return ProductDTO
     */
    public function getProduct(): ProductDTO {
        return $this->product;
    }

    /**
     * @param ProductDTO $product
     *
     * @return $this
     */
    public function setProduct(ProductDTO $product): self {
        $this->product = $product;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     *
     * @return $this
     */
    public function setQuantity(int $quantity): self {
        $this->quantity = $quantity;
        return $this;
    }
}