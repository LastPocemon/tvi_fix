<?php

namespace App\Shop\Domain\DTO;

/**
 * Данные заказа
 */
class OrderDTO {

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $status;

    /**
     * @var int
     */
    private $price;

    /**
     * @var OrderProductDTO[]
     */
    private $products;

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId(int $id): self {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return $this
     */
    public function setStatus(string $status): self {
        $this->status = $status;
        return $this;
    }

    /**
     * @return int
     */
    public function getPrice(): int {
        return $this->price;
    }

    /**
     * @param int $price
     *
     * @return $this
     */
    public function setPrice(int $price): self {
        $this->price = $price;
        return $this;
    }

    /**
     * @return OrderProductDTO[]
     */
    public function getProducts(): array {
        return $this->products;
    }

    /**
     * @param OrderProductDTO[] $products
     *
     * @return $this
     */
    public function setProducts(array $products): self {
        $this->products = $products;
        return $this;
    }
}