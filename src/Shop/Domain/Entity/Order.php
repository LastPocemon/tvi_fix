<?php

namespace App\Shop\Domain\Entity;

use App\Shop\Domain\Enum\OrderStatusEnum;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use TypeError;

/**
 * @ORM\Entity()
 * @ORM\Table(name="orders")
 */
class Order {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=10)
     *
     * @var string
     */
    private $status = OrderStatusEnum::STATUS_NEW;

    /**
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $price;

    /**
     * @ORM\OneToMany(targetEntity="OrderProduct", mappedBy="order", cascade={"persist", "remove"})
     *
     * @var Collection<int, OrderProduct>
     */
    private $products;

    /**
     * Order constructor.
     */
    public function __construct() {
        $this->products = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId(int $id): self {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string {
        return $this->status;
    }

    /**
     * @return int
     */
    public function getPrice(): int {
        return $this->price ?? 0;
    }

    /**
     * @return OrderProduct[]
     */
    public function getProducts(): array {
        return $this->products->toArray();
    }

    /**
     * @param OrderProduct $products
     *
     * @return $this
     */
    public function setProducts(OrderProduct ...$products): self {
        foreach ($products as $product) {
            $this->products->add($product);
        }
        $this->calculatePrice();
        return $this;
    }

    /**
     * Подсчитывает общую стоимость заказа по списку продуктов
     */
    private function calculatePrice(): void {
        $price = 0;
        foreach ($this->products as $product) {
            $price += ($product->getQuantity() * $product->getProduct()->getPrice());
        }
        $this->price = $price;
    }
}