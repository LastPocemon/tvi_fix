<?php

namespace App\Payment\App\Service;

use App\Payment\App\Exception\PaymentException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Сервис оплаты
 */
class PayService {

    /** код для исключений */
    private const EXCEPTION_CODE = 42;

    /** @var Client */
    private $client;

    /**
     * @param Client $client
     */
    public function __construct(Client $client) {
        $this->client = $client;
    }

    /**
     * Имитация оплаты
     *
     * @param int $sum
     *
     * @throws PaymentException
     */
    public function pay(int $sum): void {
        try {
            $response = $this->client->get('http://ya.ru/');
            if ($response->getStatusCode() !== Response::HTTP_OK) {
                throw new PaymentException('Ошибка оплаты', self::EXCEPTION_CODE);
            }
        } catch (GuzzleException $e) {
            throw new PaymentException('Ошибка оплаты', self::EXCEPTION_CODE, $e);
        }
    }
}