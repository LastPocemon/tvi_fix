<?php

namespace App\Payment\App\Service;

use App\Core\Exception\AppServiceException;
use App\Exception\AppServiceBadRequestException;
use App\Payment\App\Exception\PaymentException;
use App\Payment\Domain\Service\Exception\PaymentDomainServiceException;
use App\Payment\Domain\Service\OrderPaymentService;
use Monolog\Logger;

/**
 * Сервис для работы с оплатами
 */
class PaymentService {

    /** @var OrderPaymentService */
    private $orderPaymentService;

    /** @var PayService */
    private $payService;

    /** @var Logger */
    private $logger;

    /**
     * @param OrderPaymentService $orderPaymentService
     * @param PayService          $payService
     * @param Logger $logger
     */
    public function __construct(
        OrderPaymentService $orderPaymentService,
        PayService $payService,
        Logger $logger
    ) {
        $this->orderPaymentService = $orderPaymentService;
        $this->payService = $payService;
        $this->logger = $logger;
    }

    /**
     * Оплата заказа
     *
     * @param int $orderId
     * @param int $sum
     *
     * @throws AppServiceException
     * @throws AppServiceBadRequestException
     */
    public function paymentOrder(int $orderId, int $sum): void {
        try {
            $orderDTO = $this->orderPaymentService->toPayment($orderId, $sum);
        } catch (PaymentDomainServiceException $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
            throw new AppServiceBadRequestException($e->getMessage());
        }

        try {
            $this->payService->pay($orderDTO->getPrice());
        } catch (PaymentException $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
            throw new AppServiceException($e->getMessage());
        }

        try {
            $this->orderPaymentService->asPaid($orderDTO);
        } catch (PaymentDomainServiceException $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
            throw new AppServiceException($e->getMessage());
        }
    }
}