<?php

namespace App\Payment\App\Controller;

use App\Controller;
use App\Core\Exception\AppServiceException;
use App\Exception\AppServiceBadRequestException;
use App\JsonResponseFormatter;
use App\Payment\App\Service\PaymentService;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use TypeError;

/**
 * Контроллер для оплаты
 */
class PaymentController extends Controller {

    /** @var PaymentService  */
    public $paymentService;

    /**
     * @param PaymentService $paymentService
     */
    public function __construct(PaymentService $paymentService) {
        $this->paymentService = $paymentService;
    }

    /**
     * Оплата заказа
     *
     * @return JsonResponse
     */
    public function paymentOrder(): JsonResponse {
        try {
            [$orderId, $sum] = $this->getRequestParamForPaymentOrder();
            $this->paymentService->paymentOrder(
                $orderId,
                $sum
            );
            return JsonResponseFormatter::ok([]);
        } catch (AppServiceException $e) {
            return JsonResponseFormatter::internalServerError($e->getMessage());
        } catch (AppServiceBadRequestException $e) {
            throw new BadRequestException($e->getMessage());
        } catch (TypeError $e) {
            throw new BadRequestException('Невалидные параметры запроса');
        }
    }

    /**
     * Получение параметров для оплаты заказа
     *
     * @return array<int, int>
     * @throws BadRequestException
     */
    private function getRequestParamForPaymentOrder(): array {
        if (!$this->request->attributes->has('orderId')) {
            throw new BadRequestException('В теле запроса отсутствует параметр "orderId"');
        }
        if (!$this->request->attributes->has('sum')) {
            throw new BadRequestException('В теле запроса отсутствует параметр "sum"');
        }

        $orderId = $this->request->attributes->get('orderId');
        $sum = $this->request->attributes->get('sum');

        if (!is_int($orderId) || $orderId < 1) {
            throw new BadRequestException('Параметр "orderId" невалиден');
        }

        if (!is_int($sum)) {
            throw new BadRequestException('Параметр "sum" невалиден');
        }

        return [
            $orderId,
            $sum
        ];
    }
}