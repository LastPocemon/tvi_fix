<?php

namespace App\Payment\Domain\DTO;

/**
 * Данные заказа
 */
class OrderDTO {

    /** @var int */
    private $id;

    /** @var string */
    private $status;

    /** @var int */
    private $price;

    /**
     * @param int    $id
     * @param string $status
     * @param int    $price
     */
    public function __construct(
        int $id,
        string $status,
        int $price
    ) {
        $this->id = $id;
        $this->status = $status;
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getStatus(): string {
        return $this->status;
    }

    /**
     * @return int
     */
    public function getPrice(): int {
        return $this->price;
    }
}