<?php

namespace App\Payment\Domain\Repository;

use App\Payment\Domain\Entity\Order;
use App\Payment\Domain\Repository\Exception\RepositorySaveException;
use Doctrine\ORM\EntityManagerInterface;
use Throwable;

/**
 * Репозиторий хранения продуктов (Doctrine)
 */
class OrderRepository implements OrderRepositoryInterface {

    /** код для исключений */
    private const EXCEPTION_CODE = 3;

    /** имя сущности для исключений */
    private const ENTITY_NAME = 'Order';

    /** @var EntityManagerInterface */
    private $entityRepository;

    /**
     * @param EntityManagerInterface $entityRepository
     */
    public function __construct(EntityManagerInterface $entityRepository) {
        $this->entityRepository = $entityRepository;
    }

    /**
     * Получение заказа
     *
     * @param int $orderId
     *
     * @return Order|null
     */
    public function findById(int $orderId): ?Order {
        return $this->entityRepository->find(Order::class, $orderId);
    }

    /**
     * @inheritDoc
     */
    public function save(Order $order): void {
        try {
            $this->entityRepository->persist($order);
            $this->entityRepository->flush();
        } catch (Throwable $e) {
            throw new RepositorySaveException(
                self::ENTITY_NAME,
                self::EXCEPTION_CODE,
                $e
            );
        }
    }
}