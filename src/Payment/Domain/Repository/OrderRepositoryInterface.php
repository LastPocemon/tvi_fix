<?php

namespace App\Payment\Domain\Repository;

use App\Payment\Domain\Entity\Order;
use App\Payment\Domain\Repository\Exception\RepositorySaveException;

/**
 * Интерфейс репозитория для сущности заказ
 */
interface OrderRepositoryInterface {

    /**
     * Получение заказа
     *
     * @param int $orderId
     *
     * @return Order
     */
    public function findById(int $orderId): ?Order;

    /**
     * Сохранение заказа
     *
     * @param Order $order
     *
     * @throws RepositorySaveException
     */
    public function save(Order $order): void;

}