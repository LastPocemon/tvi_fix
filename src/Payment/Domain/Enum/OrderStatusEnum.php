<?php

namespace App\Payment\Domain\Enum;

/**
 * Статусы заказа
 */
class OrderStatusEnum {

    /** Статус заказа НОВЫЙ */
    public const STATUS_NEW = 'new';

    /** Статус заказа ОПЛАЧЕН */
    public const STATUS_PAID = 'paid';

}