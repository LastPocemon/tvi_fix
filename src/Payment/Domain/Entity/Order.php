<?php

namespace App\Payment\Domain\Entity;

use App\Payment\Domain\Enum\OrderStatusEnum;
use Doctrine\ORM\Mapping as ORM;
use DomainException;

/**
 * @ORM\Entity()
 * @ORM\Table(name="orders")
 */
class Order {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=10)
     *
     * @var string
     */
    private $status;

    /**
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $price;

    /**
     * @param int    $id
     * @param string $status
     * @param int    $price
     */
    public function __construct(int $id, string $status, int $price) {
        $this->id = $id;
        $this->status = $status;
        $this->price = $price;
    }

    /**
     * Проверка состояния заказа и суммы для возможности оплаты
     *
     * @param int $sum
     *
     * @throws DomainException
     */
    public function checkToPayment(int $sum): void {
        if ($this->status !== OrderStatusEnum::STATUS_NEW) {
            throw new DomainException('Заказ не может быть оплачен');
        }
        if ($this->price !== $sum) {
            throw new DomainException('Оплачиваемая сумма не соответствует стоимости заказа');
        }
    }

    /**
     * Переход в состояние Оплачено
     *
     * @return $this
     * @throws DomainException
     */
    public function paid(): self {
        if ($this->status !== OrderStatusEnum::STATUS_NEW) {
            throw new DomainException('Заказ не может быть помечен как оплаченный');
        }
        $this->status = OrderStatusEnum::STATUS_PAID;

        return $this;
    }

    /**
     * @return bool
     */
    public function isPaid(): bool {
        return $this->status === OrderStatusEnum::STATUS_PAID;
    }

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getStatus(): string {
        return $this->status;
    }

    /**
     * @return int
     */
    public function getPrice(): int {
        return $this->price;
    }
}