<?php

namespace App\Payment\Domain\Service;

use App\Payment\Domain\Enum\OrderStatusEnum;
use App\Payment\Domain\Service\Exception\PaymentDomainServiceException;
use App\Payment\Domain\Repository\Exception\RepositorySaveException;
use App\Payment\Domain\Repository\OrderRepositoryInterface;
use App\Payment\Domain\DTO\OrderDTO;
use DomainException;

/**
 * Сервис для взаимодействия с заказом для оплаты
 */
class OrderPaymentService {

    /** @var OrderRepositoryInterface */
    private $orderRepository;

    /**
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(OrderRepositoryInterface $orderRepository) {
        $this->orderRepository = $orderRepository;
    }

    /**
     * Проверка возможности оплаты
     *
     * @param int $orderId
     * @param int $sum
     *
     * @return OrderDTO
     * @throws PaymentDomainServiceException
     */
    public function toPayment(int $orderId, int $sum): OrderDTO {
        $order = $this->orderRepository->findById($orderId);
        if (is_null($order)) {
            throw new PaymentDomainServiceException('Отсутствует заказ с данным ID');
        }
        try {
            $order->checkToPayment($sum);
        } catch (DomainException $e) {
            throw new PaymentDomainServiceException($e->getMessage(), $e);
        }

        return new OrderDTO(
            $order->getId(),
            $order->getStatus(),
            $order->getPrice()
        );
    }

    /**
     * Смена статуса заказа на ОПЛАЧЕНО
     *
     * @param OrderDTO $orderDTO
     *
     * @return bool
     * @throws PaymentDomainServiceException
     */
    public function asPaid(OrderDTO $orderDTO): bool {
        $order = $this->orderRepository->findById($orderDTO->getId());
        if (is_null($order)) {
            throw new PaymentDomainServiceException('Отсутствует заказ с данным ID');
        }
        try {
            $this->orderRepository->save($order->paid());
        } catch (RepositorySaveException $e) {
            throw new PaymentDomainServiceException('Ошибка сохранения оплаты', $e);
        } catch (DomainException $e) {
            throw new PaymentDomainServiceException('Ошибка сохранения оплаты', $e);
        }
        return $order->isPaid();
    }
}