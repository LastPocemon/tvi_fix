<?php

namespace App\Payment\Domain\Service\Exception;

use Exception;
use Throwable;

/**
 * Исключение сервиса уровня бизнес-логики оплаты
 */
class PaymentDomainServiceException extends Exception {

    /**
     * @param string    $message
     * @param Throwable $previous
     */
    public function __construct(string $message, Throwable $previous = null) {
        parent::__construct($message, 0, $previous);
    }
}