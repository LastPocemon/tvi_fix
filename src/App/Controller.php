<?php

namespace App;

use Symfony\Component\HttpFoundation\Request;

abstract class Controller {

    /**
     * @var Request
     */
    protected $request;

    /**
     * @param Request $request
     *
     * @return $this
     */
    public function setRequest(Request $request): self {
        $this->request = $request;
        return $this;
    }
}