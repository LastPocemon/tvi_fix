<?php

namespace App\Core\Exception;

use Exception;
use Throwable;

/**
 * Исключения сервиса уровня приложения
 */
class AppServiceException extends Exception {

}