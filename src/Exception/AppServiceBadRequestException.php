<?php

namespace App\Exception;

use Exception;

/**
 * Исключения сервиса уровня приложения из-за некоректных входящих данных
 */
class AppServiceBadRequestException extends Exception {

}