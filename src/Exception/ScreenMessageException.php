<?php

namespace App\Core\Exception;

use Exception;

/**
 * Абстрактное исключение с добавленным полем ошибки для пользователя
 */
abstract class ScreenMessageException extends Exception {

    /** @var string */
    protected $screenMessage;

    /**
     * @return string
     */
    public function getScreenMessage(): string {
        return $this->screenMessage;
    }
}