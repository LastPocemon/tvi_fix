<?php

require 'vendor/autoload.php';

use Doctrine\DBAL\DriverManager;
use Doctrine\Migrations\Configuration\Migration\YamlFile;
use Doctrine\Migrations\Configuration\Connection\ExistingConnection;
use Doctrine\Migrations\DependencyFactory;
use Symfony\Component\Dotenv\Dotenv;


define('ROOT_DIR', realpath(__DIR__ . '/..'));
(new Dotenv())->bootEnv(ROOT_DIR . '/.env');

return DependencyFactory::fromConnection(
    new YamlFile('config/migrations.yaml'),
    new ExistingConnection(DriverManager::getConnection([
        'dbname'   => 'tvi',
        'user'     => $_ENV['MYSQL_USER'],
        'password' => $_ENV['MYSQL_PASSWORD'],
        'host'     => 'tvi_db',
        'driver'   => 'pdo_mysql',
    ]))
);