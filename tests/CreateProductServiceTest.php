<?php

use App\Shop\Domain\DTO\ProductDTO;
use App\Shop\Domain\Entity\Product;
use App\Shop\Domain\Factory\ProductDTOFactory;
use App\Shop\Domain\Repository\Exception\RepositorySaveException;
use App\Shop\Domain\Repository\ProductRepositoryInterface;
use App\Shop\Domain\Service\CreateProductService;
use App\Shop\Domain\Service\Exception\DomainServiceException;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * Тест сервиса создания продукта
 */
class CreateProductServiceTest extends TestCase {

    /** @var CreateProductService */
    private $createProductService;

    /** @var ProductRepositoryInterface|MockObject*/
    private $productRepositoryMock;

    /**
     * @dataProvider successDataProvider
     *
     * @param ProductDTO $productDTO
     * @param int        $expectedProductId
     *
     * @throws DomainServiceException
     */
    public function testCreate(ProductDTO $productDTO, int $expectedProductId): void {
        $product = (new Product())
            ->setId($expectedProductId)
            ->setName($productDTO->getName())
            ->setPrice($productDTO->getPrice());

        $this->productRepositoryMock->expects(self::once())
            ->method('save')
            ->willReturn($product);

        $resultProductDTO = $this->createProductService->create($productDTO);

        self::assertEquals($productDTO, $resultProductDTO, 'Ожидаемый продукт не совпал с результатом');
    }

    /**
     * Ошибка сохранения в репозиторий
     *
     * @throws DomainServiceException
     */
    public function testFailedCreate(): void {
        $productDTO = ProductDTOFactory::create('тест1', 1);
        $this->productRepositoryMock->expects(self::once())
            ->method('save')
            ->willThrowException(new RepositorySaveException('Product', 42, new Exception()));

        $this->expectException(DomainServiceException::class);
        $this->createProductService->create($productDTO);
    }

    /**
     * @return array
     */
    public function successDataProvider(): array {
        return [
            [ProductDTOFactory::create('тест1', 1), 1],
            [ProductDTOFactory::create('тест2', 441), 2],
            [ProductDTOFactory::create('тест3', 222), 3],
        ];
    }

    /**
     * @inheritDoc
     */
    public function setUp(): void {
        $this->productRepositoryMock = $this->createMock(ProductRepositoryInterface::class);
        $this->createProductService = new CreateProductService($this->productRepositoryMock);
    }
}
