<?php

use App\Payment\Domain\DTO\OrderDTO;
use App\Payment\Domain\Entity\Order;
use App\Payment\Domain\Enum\OrderStatusEnum;
use App\Payment\Domain\Repository\Exception\RepositorySaveException;
use App\Payment\Domain\Repository\OrderRepositoryInterface;
use App\Payment\Domain\Service\Exception\PaymentDomainServiceException;
use App\Payment\Domain\Service\OrderPaymentService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * Тест сервиса оплаты заказа
 */
class OrderPaymentServiceTest extends TestCase {

    /** @var OrderRepositoryInterface|MockObject*/
    private $orderRepositoryMock;

    /** @var OrderPaymentService */
    private $orderPaymentService;

    /**
     * Тест получения заказа для оплаты
     *
     * @dataProvider successToPaymentDataProvider
     *
     * @param int      $orderId
     * @param int      $sum
     *
     * @throws PaymentDomainServiceException
     */
    public function testToPayment(int $orderId, int $sum): void {
        $orderMock = new Order($orderId, OrderStatusEnum::STATUS_NEW, $sum);
        $expectedOrderDTO = new OrderDTO($orderId, OrderStatusEnum::STATUS_NEW, $sum);

        $this->orderRepositoryMock
            ->expects(self::once())
            ->method('findById')
            ->willReturn($orderMock);

        $orderDTO = $this->orderPaymentService->toPayment($orderId, $sum);

        self::assertEquals($orderDTO, $expectedOrderDTO, 'Ожидаемый заказ не совпал с результатом');
    }

    /**
     * Проверка на отсутствие заказа
     *
     * @throws PaymentDomainServiceException
     */
    public function testFailedFindOrderInToPayment(): void {
        $this->orderRepositoryMock
            ->expects(self::once())
            ->method('findById')
            ->willReturn(null);

        $this->expectException(PaymentDomainServiceException::class);
        $this->orderPaymentService->toPayment(1, 445);
    }

    /**
     * Проверка на несоответствиее суммы заказа
     *
     * @throws PaymentDomainServiceException
     */
    public function testFailedSumOrderInToPayment(): void {
        $orderMock = new Order(11, OrderStatusEnum::STATUS_NEW, 223);

        $this->orderRepositoryMock
            ->expects(self::once())
            ->method('findById')
            ->willReturn($orderMock);

        $this->expectException(PaymentDomainServiceException::class);
        $this->orderPaymentService->toPayment(11, 445);
    }

    /**
     * Проверка на несоответствиее статуса заказа
     *
     * @throws PaymentDomainServiceException
     */
    public function testFailedStatusOrderInToPayment(): void {
        $orderMock = new Order(11, OrderStatusEnum::STATUS_PAID, 223);

        $this->orderRepositoryMock
            ->expects(self::once())
            ->method('findById')
            ->willReturn($orderMock);

        $this->expectException(PaymentDomainServiceException::class);
        $this->orderPaymentService->toPayment(11, 223);
    }

    /**
     * Проверка на изменение статуса заказа после оплаты
     *
     * @dataProvider successAsPaidDataProvider
     *
     * @param OrderDTO $orderDTO
     *
     * @throws PaymentDomainServiceException
     */
    public function testAsPaid(OrderDTO $orderDTO): void {
        $this->orderRepositoryMock
            ->expects(self::once())
            ->method('findById')
            ->willReturn(new Order(
                $orderDTO->getId(),
                $orderDTO->getStatus(),
                $orderDTO->getPrice()
            ));

        self::assertEquals(
            $this->orderPaymentService->asPaid($orderDTO),
            true,
            'Результат смены статуса заказа не совпал с ожидаемым'
        );
    }

    /**
     * Проверка на отсутствия заказа
     *
     * @return array
     * @throws PaymentDomainServiceException
     */
    public function testFailedFindOrderInAsPaid(): array {
        $orderDTO = new OrderDTO(1, OrderStatusEnum::STATUS_NEW, 442);
        $this->orderRepositoryMock
            ->expects(self::once())
            ->method('findById')
            ->willReturn(null);

        $this->expectException(PaymentDomainServiceException::class);
        $this->orderPaymentService->asPaid($orderDTO);
    }

    /**
     * Проверка на некоректность статуса переданного заказа
     *
     * @return array
     * @throws PaymentDomainServiceException
     */
    public function testFailedStatusOrderInAsPaid(): array {
        $orderDTO = new OrderDTO(1, OrderStatusEnum::STATUS_PAID, 442);
        $this->orderRepositoryMock
            ->expects(self::once())
            ->method('findById')
            ->willReturn(new Order(
                $orderDTO->getId(),
                $orderDTO->getStatus(),
                $orderDTO->getPrice()
            ));

        $this->expectException(PaymentDomainServiceException::class);
        $this->orderPaymentService->asPaid($orderDTO);
    }

    /**
     * Проверка на ошибку сохранения смены статуса заказа
     *
     * @return array
     * @throws PaymentDomainServiceException
     */
    public function testFailedSaveOrderInAsPaid(): array {
        $orderDTO = new OrderDTO(1, OrderStatusEnum::STATUS_NEW, 442);
        $this->orderRepositoryMock
            ->expects(self::once())
            ->method('findById')
            ->willReturn(new Order(
                $orderDTO->getId(),
                $orderDTO->getStatus(),
                $orderDTO->getPrice()
            ));

        $this->orderRepositoryMock
            ->expects(self::once())
            ->method('save')
            ->willThrowException(new RepositorySaveException('Order', 42, new Exception()));

        $this->expectException(PaymentDomainServiceException::class);
        $this->orderPaymentService->asPaid($orderDTO);
    }

    /**
     * @return array
     */
    public function successToPaymentDataProvider(): array {
        return [
            [
                1,
                334,
            ],
            [
                2,
                3441,
            ],
        ];
    }

    /**
     * @return array
     */
    public function successAsPaidDataProvider(): array {
        return [
            [new OrderDTO(1, OrderStatusEnum::STATUS_NEW, 442)],
            [new OrderDTO(44, OrderStatusEnum::STATUS_NEW, 2212)],
        ];
    }

    /**
     * @inheritDoc
     */
    public function setUp(): void {
        $this->orderRepositoryMock = $this->createMock(OrderRepositoryInterface::class);
        $this->orderPaymentService = new OrderPaymentService(
            $this->orderRepositoryMock
        );
    }
}
