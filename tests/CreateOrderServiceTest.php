<?php

use App\Shop\Domain\DTO\OrderDTO;
use App\Shop\Domain\DTO\ProductDTO;
use App\Shop\Domain\Entity\Order;
use App\Shop\Domain\Entity\Product;
use App\Shop\Domain\Factory\OrderDTOFactory;
use App\Shop\Domain\Factory\OrderProductDTOFactory;
use App\Shop\Domain\Factory\ProductDTOFactory;
use App\Shop\Domain\Repository\Exception\RepositoryFindException;
use App\Shop\Domain\Repository\Exception\RepositorySaveException;
use App\Shop\Domain\Repository\OrderRepositoryInterface;
use App\Shop\Domain\Repository\ProductRepositoryInterface;
use App\Shop\Domain\Service\CreateOrderService;
use App\Shop\Domain\Service\Exception\DomainBadRequestException;
use App\Shop\Domain\Service\Exception\DomainServiceException;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * Тест сервиса создания заказа
 */
class CreateOrderServiceTest extends TestCase {

    /** @var OrderRepositoryInterface|MockObject */
    private $orderRepositoryMock;

    /** @var ProductRepositoryInterface|MockObject */

    private $productRepositoryMock;

    /** @var CreateOrderService */
    private $createOrderService;

    /**
     * Тест создания заказа
     *
     * @dataProvider successDataProvider
     *
     * @param OrderDTO  $orderDTO
     * @param Product[] $productMocks
     * @param int       $expectedOrderId
     *
     * @throws DomainServiceException
     * @throws DomainBadRequestException
     */
    public function testCreate(OrderDTO $orderDTO, array $productMocks, int $expectedOrderId): void {
        $this->productRepositoryMock
            ->expects(self::once())
            ->method('findManyByIds')
            ->willReturn($productMocks);

        $orderMock = (new Order())
            ->setId($expectedOrderId);

        $this->orderRepositoryMock
            ->expects(self::once())
            ->method('save')
            ->willReturn($orderMock);

        $resultOrderDTO = $this->createOrderService->create($orderDTO);

        self::assertEquals($orderDTO, $resultOrderDTO, 'Ожидаемый заказ не совпал с результатом');
    }

    /**
     * Проверка обработки ошибки от репозитория Продукта
     *
     * @throws DomainServiceException
     * @throws DomainBadRequestException
     */
    public function testFailedFindProduct(): void {
        $orderDTO = OrderDTOFactory::createByOrderProducts([
            OrderProductDTOFactory::create(
                1,
                ProductDTOFactory::createById(2)
            )
        ]);

        $this->productRepositoryMock
            ->expects(self::once())
            ->method('findManyByIds')
            ->willThrowException(new RepositoryFindException('Product', 42, new Exception()));

        $this->expectException(DomainServiceException::class);
        $this->createOrderService->create($orderDTO);
    }

    /**
     * Проверка обработки ошибки от репозитория Заказа
     *
     * @throws DomainServiceException
     * @throws DomainBadRequestException
     */
    public function testFailedCreate(): void {
        $orderDTO = OrderDTOFactory::createByOrderProducts([
            OrderProductDTOFactory::create(
                1,
                ProductDTOFactory::createById(2)
            )
        ]);

        $productsMock = [(new Product())->setId(2)->setName('продукт')->setPrice(435)];

        $this->productRepositoryMock
            ->expects(self::once())
            ->method('findManyByIds')
            ->willReturn($productsMock);

        $this->orderRepositoryMock
            ->expects(self::once())
            ->method('save')
            ->willThrowException(new RepositorySaveException('Order', 42, new Exception()));

        $this->expectException(DomainServiceException::class);
        $this->createOrderService->create($orderDTO);
    }

    /**
     * @return array
     */
    public function successDataProvider(): array {
        return  [
            [
                OrderDTOFactory::createByOrderProducts(
                    [
                        OrderProductDTOFactory::create(
                            1,
                            (new ProductDTO())->setId(33)
                        ),
                    ]
                ),
                [
                    (new Product())->setId(33)->setName('продукт')->setPrice(435),
                ],
                1
            ],
            [
                OrderDTOFactory::createByOrderProducts(
                    [
                        OrderProductDTOFactory::create(
                            2,
                            (new ProductDTO())->setId(33)
                        ),
                        OrderProductDTOFactory::create(
                            1,
                            (new ProductDTO())->setId(34)
                        ),
                    ]
                ),
                [
                    (new Product())->setId(33)->setName('продукт1')->setPrice(435),
                    (new Product())->setId(34)->setName('продукт2')->setPrice(124),
                ],
                2
            ],
            [
                OrderDTOFactory::createByOrderProducts(
                    [
                        OrderProductDTOFactory::create(
                            2,
                            (new ProductDTO())->setId(33)
                        ),
                        OrderProductDTOFactory::create(
                            1,
                            (new ProductDTO())->setId(34)
                        ),
                        OrderProductDTOFactory::create(
                            3,
                            (new ProductDTO())->setId(35)
                        ),
                    ]
                ),
                [
                    (new Product())->setId(33)->setName('продукт1')->setPrice(435),
                    (new Product())->setId(34)->setName('продукт2')->setPrice(124),
                    (new Product())->setId(35)->setName('продукт3')->setPrice(33),
                ],
                2
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function setUp(): void {
        $this->orderRepositoryMock = $this->createMock(OrderRepositoryInterface::class);
        $this->productRepositoryMock = $this->createMock(ProductRepositoryInterface::class);
        $this->createOrderService = new CreateOrderService(
            $this->orderRepositoryMock,
            $this->productRepositoryMock
        );
    }
}
